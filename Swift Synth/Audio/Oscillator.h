//
//  Oscillator.h
//  Swift Synth
//
//  Created by Ivan Ornes on 23/11/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef float (^Signal)(float);

typedef NS_ENUM(NSInteger, Waveform) {
    sine,
    triangle,
    sawtooth,
    square,
    whiteNoise
};

@interface Oscillator : NSObject
+ (float)getAmplitude;
+ (void)setAmplitude:(float) amplitude;
+ (float)getFrequency;
+ (void)setFrequency:(float) frequency;
+ (Signal)sine;
+ (Signal)triangle;
+ (Signal)sawtooth;
+ (Signal)square;
+ (Signal)whiteNoise;
@end

NS_ASSUME_NONNULL_END
