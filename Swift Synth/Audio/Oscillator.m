//
//  Oscillator.m
//  Swift Synth
//
//  Created by Ivan Ornes on 23/11/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Oscillator.h"
#import <Foundation/Foundation.h>

@implementation Oscillator
static float amplitude = 1;
static float frequency = 1;

+ (float)getAmplitude {
    return amplitude;
}

+ (void)setAmplitude:(float) amp {
    amplitude = amp;
}

+ (float)getFrequency {
    return frequency;
}

+ (void)setFrequency:(float) freq {
    frequency = freq;
}

+ (Signal)sine {
    return ^float(float time) {
        return amplitude * sin(2.0 * M_PI * frequency * time);
    };
}

+ (Signal)triangle {
    return ^float(float time) {
        double period = 1.0 / (double) frequency;
        double currentTime = fmod((double) time, period);
    
        double value = currentTime / period;
    
        double result = 0.0;
    
        if (value < 0.25) {
            result = value * 4;
        } else if (value < 0.75) {
            result = 2.0 - (value * 4.0);
        } else {
            result = value * 4 - 4.0;
        }
    
        return amplitude * (float)result;
    };
}

+ (Signal)sawtooth {
    return ^float(float time) {
        double period = 1.0 / frequency;
        double currentTime = fmod((double) time, period);
    
        return amplitude * (((float) currentTime / period) * 2 - 1.0);
    };
}

+ (Signal)square {
    return ^float(float time) {
        double period = 1.0 / (double) frequency;
        double currentTime = fmod((double) time, period);
    
        return ((currentTime / period) < 0.5) ? amplitude : -1.0 * amplitude;
    };
}

+ (Signal)whiteNoise {
    return ^float(float time) {
        return amplitude * (arc4random() % 2) - 1;
    };
}

@end
