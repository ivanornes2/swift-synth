//
//  Synth.h
//  Swift Synth
//
//  Created by Ivan Ornes on 23/11/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Oscillator.h"

NS_ASSUME_NONNULL_BEGIN

@class Synth;

@protocol SynthDelegate <NSObject>
@optional
- (void)synth:(Synth *)synth didChangePlayingState: (BOOL) isPlaying;
@end

@interface Synth : NSObject
@property (nonatomic, weak) id <SynthDelegate> delegate;

+ (instancetype)shared;
- (float)getVolume;
- (void)setVolume:(float)volume;
- (BOOL)isPlayingAudio;
- (void)setWaveformTo:(Signal)signal;
@end

NS_ASSUME_NONNULL_END
