//
//  Synth.m
//  Swift Synth
//
//  Created by Ivan Ornes on 23/11/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Synth.h"
#import <AVFoundation/AVFoundation.h>

@implementation Synth

+ (instancetype)shared
{
    static Synth *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Synth alloc] initWithSignal:Oscillator.sine];
    });
    return sharedInstance;
}

- (void)setVolume:(float)volume {
    audioEngine.mainMixerNode.outputVolume = volume;
    
    [_delegate synth:self didChangePlayingState: [self isPlayingAudio]];
}

- (float)getVolume {
    return audioEngine.mainMixerNode.outputVolume;
}

- (BOOL)isPlayingAudio {
    return audioEngine.mainMixerNode.outputVolume > 0;
}

AVAudioEngine *audioEngine;
AVAudioSourceNode *_sourceNode;

- (AVAudioSourceNode *)sourceNode {
    if (!_sourceNode) {
        _sourceNode = [[AVAudioSourceNode alloc]
                                    initWithRenderBlock:
                                    ^OSStatus(BOOL * _Nonnull isSilence,
                                              const AudioTimeStamp * _Nonnull timestamp,
                                              AVAudioFrameCount frameCount,
                                              AudioBufferList * _Nonnull audioBufferList) {
                          for(int frame=0; frame<frameCount; frame++) {
                              float sampleVal = _signal(_time);
                              _time += deltaTime;
                              for(int bufferIndex=0; bufferIndex<audioBufferList->mNumberBuffers; bufferIndex++) {
                                  Float32 *audioBuffer = (Float32 *) audioBufferList->mBuffers[bufferIndex].mData;
                                  audioBuffer[frame] = sampleVal;
                              }
                          }
                          return noErr;
                      }];
    }
    return _sourceNode;
}

float _time = 0;
double sampleRate;
float deltaTime;

Signal _signal;

- (id)initWithSignal:(Signal) signal {
    self = [super init];
    if (self) {
        audioEngine = [[AVAudioEngine alloc] init];
        
        AVAudioMixerNode *mainMixer = audioEngine.mainMixerNode;
        AVAudioOutputNode *outputNode = audioEngine.outputNode;
        AVAudioFormat *format = [outputNode inputFormatForBus: 0];
        
        sampleRate = format.sampleRate;
        deltaTime = 1 / (float) sampleRate;
        
        _signal = signal;
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat
                                                                      sampleRate:sampleRate
                                                                        channels:1
                                                                     interleaved:format.isInterleaved];
        [audioEngine attachNode: [self sourceNode]];
        [audioEngine connect:[self sourceNode] to:mainMixer format:inputFormat];
        [audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;
        [mainMixer addObserver:self forKeyPath:@"outputVolume" options: NSKeyValueObservingOptionNew context: nil];
        
        NSError *error;
        [audioEngine startAndReturnError:&error];
        if (error != nil) {
            NSLog(@"Could not start audioEngine: %@", [error localizedDescription]);
        }
    }
    return self;
}

- (void)setWaveformTo:(Signal) signal {
    _signal = signal;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    float newValue = [[change objectForKey:NSKeyValueChangeNewKey] floatValue];
    [_delegate synth:self didChangePlayingState:newValue > 0];
}
@end
